
////////////////////////////////////////////////////////////////////////////////
//
//                W R I T T E N   B Y   I M P E R A S   I G E N
//
//                             Version 20161005.0
//
////////////////////////////////////////////////////////////////////////////////


#include <string.h>
#include <stdlib.h>

#include "op/op.h"


#define MODULE_NAME "FIM_BAREMETAL"


typedef struct optModuleObjectS {
    // insert module persistent data here
} optModuleObject;


////////////////////////////////////////////////////////////////////////////////
//                         U S E R   F U N C T I O N S
////////////////////////////////////////////////////////////////////////////////

// forward declaration of component constructor
static OP_CONSTRUCT_FN(instantiateComponents);

static OP_CONSTRUCT_FN(moduleConstructor) {

    // instantiate module components
    instantiateComponents(mi, object);

    // insert constructor code here
}

static OP_PRE_SIMULATE_FN(modulePreSimulate) {
// insert pre simulation code here
}

static OP_SIMULATE_STARTING_FN(moduleSimulateStart) {
// insert simulation starting code here
}

static OP_POST_SIMULATE_FN(modulePostSimulate) {
// insert post simulation  code here
}

static OP_DESTRUCT_FN(moduleDestruct) {
// insert destructor code here
}

#include "module.igen.h"
