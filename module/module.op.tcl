#
# Copyright (c) 2005-2016 Imperas Software Ltd., www.imperas.com
#
# The contents of this file are provided under the Software License
# Agreement that you accepted before downloading this file.
#
# This source forms part of the Software and can be used for educational,
# training, and demonstration purposes but cannot be used for derivative
# works except in cases where the derivative works require OVP technology
# to run.
#
# For open source models released under licenses that you can use for
# derivative works, please visit www.OVPworld.org or www.imperas.com
# for the location of the open source models.
#

# Setup variables for platform info
set vendor  arm.ovpworld.org
set library module
set name    FIM_BAREMETAL
set version 1.0


#~ ihwnew -name simpleCpuMemory
ihwnew -name $name -stoponctrlc


for {set i 0} {$i < $::env(CPUS) } {incr i} {
    #Processor
    ihwaddprocessor -instancename cpu${i} -type armm -vendor $vendor -version $version -endian little -semihostname armNewlib -mips 100.0

    #Main Bus
    ihwaddbus       -instancename mainBus${i}   -addresswidth 32

    #Connections --> MPx cores
    ihwconnect -bus mainBus${i} -instancename cpu${i} -busmasterport INSTRUCTION
    ihwconnect -bus mainBus${i} -instancename cpu${i} -busmasterport DATA

    #FLASH
    ihwaddmemory -instancename flash${i} -type ram
    ihwconnect -bus mainBus${i} -instancename flash${i} -busslaveport sp1 -loaddress 0x0 -hiaddress 0x0000ffff

    #RAM
    ihwaddmemory -instancename ram${i} -type ram
    ihwconnect -bus mainBus${i} -instancename ram${i} -busslaveport sp1 -loaddress 0x20000000 -hiaddress 0x200fffff
    
    if {$i == 0} {
        #REPOSITORY
        ihwaddmemory -instancename repository -type ram
        ihwconnect -bus mainBus${i} -instancename repository -busslaveport sp1 -loaddress 0x60000000 -hiaddress 0x600fffff
    }
}
