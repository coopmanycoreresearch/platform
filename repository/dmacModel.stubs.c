#include "dmacModel.h"
#include <stdio.h>
#include <stdlib.h>
//#include "build/ids_master.h"
#include "repository.h"

//#define INSERTION_LOAD    0.8
#define MIN_LOAD    0.2
#define MAX_LOAD    0.7

#define THREAD_STACK      (8*1024)

float mapped_tasks = 0;
float finished_tasks = 0;
int app_index = 0;
unsigned int app_address = 0;
unsigned int app_size = 0;
int wait_load = 0;

typedef struct {
    bhmThreadHandle       thread;
    bhmEventHandle        start;
    bhmEventHandle        system_load_ok;
    bhmEventHandle        ack_posedge_event;
    bhmEventHandle        ack_negedge_event;
    char                  stack[THREAD_STACK];
} th;

static th thAppInsertion;

Uns32 swap_Uns32(Uns32 val)
{
    val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF );
    return (val << 16) | (val >> 16);
}

PPM_CONSTRUCTOR_CB(constructor) {
    // YOUR CODE HERE (pre constructor)
    periphConstructor();
    userInit();
    // YOUR CODE HERE (post constructor)
}

PPM_DESTRUCTOR_CB(destructor) {
    // YOUR CODE HERE (destructor)
}

void ack_received(ppmNetValue new, void *userdata)
{
    if (ppmReadNet(handles.ack_app)==1) bhmTriggerEvent(thAppInsertion.ack_posedge_event);
    if (ppmReadNet(handles.ack_app)==0) bhmTriggerEvent(thAppInsertion.ack_negedge_event);
}

void update_finished_tasks(ppmNetValue new, void *userdata)
{
    float load;
    finished_tasks = ppmReadNet(handles.finished_tasks);
    load = (mapped_tasks-finished_tasks)/MAX_GLOBAL_TASKS;
    //printf("==============================> CURRENT_TASKS %d - %d = %d\n", (int)mapped_tasks, (int)finished_tasks, (int)(mapped_tasks - finished_tasks));
    //if (wait_load==1) printf("\nyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy LOAD %f\n", load);
    if (load<=MIN_LOAD && wait_load==1) {
        wait_load = 0;
        bhmTriggerEvent(thAppInsertion.system_load_ok);
        //printf("OK!!!!!!!!!\n");
    }
}

static void apps_dynamic_insertion(void *user)
{
    //printf("ANTES\n");
    //bhmWaitEvent(thAppInsertion.start);
    //printf("DEPOIS\n");
    float load = 0;
    int j = 0;
    //int total_tasks;
    //int app=0;

    //if (load>INSERTION_LOAD) bhmWaitEvent(thAppInsertion.system_load_ok);
    if (load>=MAX_LOAD) {
        wait_load = 1;
        bhmWaitEvent(thAppInsertion.system_load_ok);
    }
    //bhmWaitDelay(appstime[j]);
    //total_tasks = repository[0] + dynamic_apps[j][0];
    //ppmWriteNet(handles.app_size, total_tasks);
    ppmWriteNet(handles.req_app, j);
    //printf("Pediu a aplicacao %d\n", j);
    bhmWaitEvent(thAppInsertion.ack_posedge_event);
    //printf("Recebeu ack!\n");
    ppmWriteNet(handles.req_app, 0);
    bhmWaitEvent(thAppInsertion.ack_negedge_event);
    //printf("ack negado!\n");
    mapped_tasks = mapped_tasks + repository[apps_addresses[appstype[j]]];
    load = (mapped_tasks-finished_tasks)/MAX_GLOBAL_TASKS;
    //printf("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> %f %f %d : LOAD %f\n", mapped_tasks, finished_tasks, MAX_GLOBAL_TASKS, load);

}

void updateAppIndex(ppmNetValue new, void *userdata){
    app_index = ppmReadNet(handles.app_index);
    app_address = apps_addresses[appstype[app_index]];
    app_size = repository[apps_addresses[appstype[app_index]]];
    ppmWriteNet(handles.app_addr,app_address);
    ppmWriteNet(handles.app_size,app_size);
}

void userInit(void)
{
    Uns32 oldValue = -1;

    ppmInstallNetCallback(handles.ack_app, ack_received, &oldValue);
    ppmInstallNetCallback(handles.req_app, ack_received, &oldValue);
    ppmInstallNetCallback(handles.app_index, updateAppIndex, &oldValue);
    ppmInstallNetCallback(handles.finished_tasks, update_finished_tasks, &oldValue);

    thAppInsertion.start = bhmCreateEvent();
    thAppInsertion.system_load_ok = bhmCreateEvent();
    thAppInsertion.ack_posedge_event = bhmCreateEvent();
    thAppInsertion.ack_negedge_event = bhmCreateEvent();
    thAppInsertion.thread = bhmCreateThread(
            apps_dynamic_insertion,
            NULL,
            "thAppInsertion",
            &thAppInsertion.stack[THREAD_STACK] // top of downward growing stack
            );

    if (NUMBER_OF_APPS!=0) {
        //printf("START!!!!\n");
        bhmTriggerEvent(thAppInsertion.start);
    }
    else
    {
        printf("NOT TRIGGER EVENT!!!!!!\n");
    }
}

