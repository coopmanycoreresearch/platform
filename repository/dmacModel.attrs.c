/*
 *
 * Copyright (c) 2005-2013 Imperas Software Ltd., www.imperas.com
 *
 * The contents of this file are provided under the Software License
 * Agreement that you accepted before downloading this file.
 *
 * This source forms part of the Software and can be used for educational,
 * training, and demonstration purposes but cannot be used for derivative
 * works except in cases where the derivative works require OVP technology
 * to run.
 *
 * For open source models released under licenses that you can use for
 * derivative works, please visit www.OVPworld.org or www.imperas.com
 * for the location of the open source models.
 *
 */


////////////////////////////////////////////////////////////////////////////////
//
//                W R I T T E N   B Y   I M P E R A S   I G E N
//
//                          Fri Nov  9 16:48:49 2012
//
////////////////////////////////////////////////////////////////////////////////


#ifdef _PSE_
#    include "peripheral/impTypes.h"
#    include "peripheral/bhm.h"
#    include "peripheral/ppm.h"
#else
#    include "hostapi/impTypes.h"
#endif

static ppmNetPort netPorts[] = {
    {
        .name            = "req_app",
        .type            = PPM_OUTPUT_PORT,
        .mustBeConnected = 0,
        .description     = "req app"
    },
    {
        .name            = "ack_app",
        .type            = PPM_INPUT_PORT,
        .mustBeConnected = 0,
        .description     = "ack app"
    },
    {
        .name            = "app_index",
        .type            = PPM_INPUT_PORT,
        .mustBeConnected = 0,
        .description     = "app index"
    },
    {
        .name            = "app_size",
        .type            = PPM_OUTPUT_PORT,
        .mustBeConnected = 0,
        .description     = "app size"
    },
    {
        .name            = "app_addr",
        .type            = PPM_OUTPUT_PORT,
        .mustBeConnected = 0,
        .description     = "app_address"
    },
    {
        .name            = "finished_tasks",
        .type            = PPM_INPUT_PORT,
        .mustBeConnected = 0,
        .description     = "finished tasks"
    },
    { 0 }
};

static PPM_NET_PORT_FN(nextNetPort) {
    if (!netPort) {
        return netPorts;
    }
    netPort++;
    return netPort->name ? netPort : 0;
}

static ppmParameter parameters[] = {
    { 0 }
};

static PPM_PARAMETER_FN(nextParameter) {
    if (!parameter) {
        return parameters;
    }
    parameter++;
    return parameter->name ? parameter : 0;
}

ppmModelAttr modelAttrs = {

    .versionString = PPM_VERSION_STRING,
    .type          = PPM_MT_PERIPHERAL,

    .netPortsCB    = nextNetPort,
    .paramSpecCB   = nextParameter,

    .vlnv          = {
        .vendor  = "ovpworld.org",
        .library = "peripheral",
        .name    = "dmac",
        .version = "1.0"
    },

    .family    = "ovpworld.org",

};
