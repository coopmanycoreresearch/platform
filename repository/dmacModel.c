#include "dmacModel.h"
#include <stdio.h>

/////////////////////////////// Port Declarations //////////////////////////////

handlesT handles;

//////////////////////////////// Bus Slave Ports ///////////////////////////////

static void installSlavePorts(void) {
    handles.req_app = ppmOpenNetPort("req_app");
    handles.ack_app = ppmOpenNetPort("ack_app");
    handles.ack_app = ppmOpenNetPort("app_index");
    handles.app_size = ppmOpenNetPort("app_size");
    handles.app_addr = ppmOpenNetPort("app_addr");
    handles.finished_tasks = ppmOpenNetPort("finished_tasks");
}

////////////////////////////////// Constructor /////////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor) {
    installSlavePorts();
}

///////////////////////////////////// Main /////////////////////////////////////

int main(int argc, char *argv[]) {
    constructor();
    bhmWaitEvent(bhmGetSystemEvent(BHM_SE_END_OF_SIMULATION));
    destructor();
    return 0;
}

