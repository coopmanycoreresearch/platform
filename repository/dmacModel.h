#ifndef DMACMODEL_H
#define DMACMODEL_H
#include "peripheral/impTypes.h"
#include "peripheral/bhm.h"
#include "peripheral/ppm.h"

///////////////////////////////// Port handles /////////////////////////////////

typedef struct handlesS {
    ppmNetHandle req_app;
    ppmNetHandle ack_app;
    ppmNetHandle app_index;
    ppmNetHandle app_size;
    ppmNetHandle app_addr;
    ppmNetHandle finished_tasks;
} handlesT, *handlesTP;

extern handlesT handles;

//! Byte swap unsigned int
Uns32 swap_Uns32(Uns32 val);
////////////////////////////// Callback prototypes /////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor);
PPM_DESTRUCTOR_CB(periphDestructor);
PPM_CONSTRUCTOR_CB(constructor);
PPM_DESTRUCTOR_CB(destructor);
//static void ReadNI(void *user);
void userInit(void);
#endif
