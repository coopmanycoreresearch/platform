#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

//System includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include "router_include.h"
#include "op/op.h"


extern optNetP  		router_address[NUMBER_OF_CPUS];
extern optNetP          end_sim[NUMBER_OF_CPUS];
extern optNetP          data_read[NUMBER_OF_CPUS][NPORT];
extern optProcessorP 	processors[NUMBER_OF_CPUS];
extern optNetP  		ni_intr[NUMBER_OF_CPUS];
extern optModuleP       topLevelModule;
extern optModuleP       platformMod;

/** Functions headers **/

OP_NET_WRITE_FN(req_app_update);

OP_NET_WRITE_FN(app_size_update);

OP_NET_WRITE_FN(app_addr_update);

void bufferReceive(int p, int value);

OP_NET_WRITE_FN(Receive);

int RouterPosition(int router);

int RouterPorts(int router);

int RouterAddress(int router);

void update_volume(int xi, int yi, int xf, int yf, int vol);

void dmaProcess(int p);

int register_bank_read(int address, int p);

void register_bank_write(int address, int value, int p);

OP_BUS_SLAVE_READ_FN(regbank_rb);

OP_BUS_SLAVE_WRITE_FN(regbank_wb);

#endif /*__CALLBACKS_H__*/
