#ifndef _FIM
#define _FIM

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "op/op.h"

///Definitions
#define BASE_MEMORY 0X00000000
#define CODE_MEMORY BASE_MEMORY
#define DATA_MEMORY 0X20000000
#define REPO_MEMORY 0X60000000
#define SIZE_CODE_MEMORY 0X0000ffff
#define SIZE_DATA_MEMORY 0X000fffff
#define SIZE_REPO_MEMORY 0Xffffffff


//Options from harness.igen.stubs
struct optionsS {
    const char*          zimage;
    Uns64                zimageaddr;
    const char*          initrd;
    Uns64                initrdaddr;
    const char*          linuxsym;
    const char*          linuxcmd;
    Int32                boardid;
    Uns64                linuxmem;
    const char*          boot;
    const char*          image0;
    Uns64                image0addr;
    const char*          image0sym;
    const char*          image1;
    Uns64                image1addr;
    const char*          image1sym;
    const char*          uart0port;
    const char*          uart1port;
    Bool                 nographics;
    Uns32                numberofcpus;
} options = {
    0
};


//
// Internal struct of each PE
//
typedef struct processorStruct {
    /// simulator objects
    Uns32           identifier;
    optProcessorP   processorObj;
    optProcessorP*  childrens;
    optMemoryP      memObj;
    optBusP         busObj;
    optModuleP      modObj;

    /// options
    struct optionsS options;

} processorDataS,*processorDataP;



#define PREFIX_FIM                        "mpsoc"
#define MACRO_NUMBER_OF_CPUS              options.numberofcpus

void initialize();
void simulate(struct optionsS _options, optModuleP _mi);
void initProcessorInst(optProcessorP processorObj);


#endif
