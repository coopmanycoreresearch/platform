#include "op/op.h"

#include "callbacks.h"
#include "network_interface.h"
#include "repository.h"
#include "mpsocPlatform.h"

int counter                     [ NUMBER_OF_CPUS ] ;
int counter_packet              [ NUMBER_OF_CPUS ] ;
int counter_send                [ NUMBER_OF_CPUS ] ;
int dma_size                    [ NUMBER_OF_CPUS ] ;
int dma_size_2                  [ NUMBER_OF_CPUS ] ;
int dma_address                 [ NUMBER_OF_CPUS ] ;
int dma_address_2               [ NUMBER_OF_CPUS ] ;
int dma_op                      [ NUMBER_OF_CPUS ] ;
int dma_2                       [ NUMBER_OF_CPUS ] ;
int dma_start                   [ NUMBER_OF_CPUS ] ;
int data_in                     [ NUMBER_OF_CPUS ] ;
int first_address               [ NUMBER_OF_CPUS ] ;
int first_read                  [ NUMBER_OF_CPUS ] ;
int header_dma_size             [ NUMBER_OF_CPUS ] ;
int hops                        [ NUMBER_OF_CPUS ] ;
int packets_in                  [ NUMBER_OF_CPUS ] ;
int packets_out                 [ NUMBER_OF_CPUS ] ;
int router_cycles               [ NUMBER_OF_CPUS ] ;
int size                        [ NUMBER_OF_CPUS ] ;
int size_packet                 [ NUMBER_OF_CPUS ] ;
int size_send                   [ NUMBER_OF_CPUS ] ;
int volume                      [ NUMBER_OF_CPUS ] ;
int x_origin                    [ NUMBER_OF_CPUS ] ;
int x_destiny                   [ NUMBER_OF_CPUS ] ;
int y_origin                    [ NUMBER_OF_CPUS ] ;
int y_destiny                   [ NUMBER_OF_CPUS ] ;

int total_hops;
int total_hops_msg;
int totalVolume;
int totalVolume_msg;

float energy                    [ NUMBER_OF_CPUS ] ;
float totalEnergy;
float totalEnergy_msg;

double max_instructions = 0;
double totalSimulated;
double elapsed;
double mips;

struct timeval stop;
struct timeval start;
struct timeval tick_counter     [ NUMBER_OF_CPUS ] ;

typedef struct node {
    int x;
    struct node *next;
}node;

node *buff[NUMBER_OF_CPUS];
node *first[NUMBER_OF_CPUS];
node *clean[NUMBER_OF_CPUS];

OP_NET_WRITE_FN( Receive )
{
     bufferReceive( ( intptr_t ) userData , value );
}

//register bank callback functions
OP_BUS_SLAVE_READ_FN( regbank_rb )
{
    int p;
    sscanf( opObjectName( initiator ) , "cpu%i" , &p );
    int readData = register_bank_read( addr , p );
    * ( Int32 * ) data = readData;
}

OP_BUS_SLAVE_WRITE_FN( regbank_wb )
{
    int p;
    sscanf( opObjectName( initiator ) , "cpu%i" , &p );
    register_bank_write( addr , ( *( Uns32* )data ) , p );
}

int NormalAddress( int normal )
{
    int n_address , pos_x , pos_y;

    pos_x = normal >> 8;
    pos_y = normal & 0xFF;

    if ( NUMBER_OF_CPUS_X <= NUMBER_OF_CPUS_Y ) {
        n_address = ( NUMBER_OF_CPUS_Y * pos_y ) + pos_x;
    } else {
        n_address = ( NUMBER_OF_CPUS_X * pos_y ) + pos_x;
    }

    return n_address;
}

void bufferReceive( int p , int value )
{
    counter[ p ]++;

    if ( first[ p ] == NULL ) {
        first[ p ] = ( node* )  malloc( sizeof( struct node ) );
        first[ p ]->x = value;
        first[ p ]->next = NULL;
        buff[ p ] = first[ p ];
    } else {
        buff[ p ]->next = ( node* )  malloc( sizeof( struct node ) );
        buff[ p ] = buff[ p ]->next;
        buff[ p ]->x = value;
        buff[ p ]->next = NULL;
    }
    if ( counter[ p ] == 2 ) {
        size[ p ] = value;
    }
    if ( counter[ p ]>2 ) {
        size[ p ]--;
        if ( size[ p ] == 0 ) {
            opNetWrite(ni_intr[ p ], 1);
            counter[ p ] = 0;
            packets_in[ p ]++;
        }
    }

}

int RouterPosition( int router )
{
    int pos;

    int column = router%NUMBER_OF_CPUS_X;

    if ( router >= ( NUMBER_OF_CPUS-NUMBER_OF_CPUS_X ) ) {
        if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
            pos = TOP_RIGHT;
        } else {
            if ( column == 0 ) {
                pos = TOP_LEFT;
            } else {
                pos = TOP_CENTER;
            }
        }
    } else {
        if ( router<NUMBER_OF_CPUS_X ) {
            if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
                pos = BOTTOM_RIGHT;
            } else {
                if ( column == 0 ) {
                    pos = BOTTOM_LEFT;
                } else {
                    pos = BOTTOM_CENTER;
                }
            }
        } else {
            if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
                pos = CENTER_RIGHT;
            } else {
                if ( column == 0 ) {
                    pos = CENTER_LEFT;
                } else {
                    pos = CENTER_CENTER;
                }
            }
        }
    }

    return pos;
}

int RouterPorts( int router )
{
    int ports;

    int column = router%NUMBER_OF_CPUS_X;

    if ( router >= ( NUMBER_OF_CPUS-NUMBER_OF_CPUS_X ) ) {
        if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
            ports = 3;
        } else {
            if ( column == 0 ) {
                ports = 3;
            } else {
                ports = 4;
            }
        }
    } else {
        if ( router<NUMBER_OF_CPUS_X ) {
            if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
                ports = 3;
            } else {
                if ( column == 0 ) {
                    ports = 3;
                } else {
                    ports = 4;
                }
            }
        } else {
            if ( column == ( NUMBER_OF_CPUS_X-1 ) ) {
                ports = 4;
            } else {
                if ( column == 0 ) {
                    ports = 4;
                } else {
                    ports = 5;
                }
            }
        }
    }

    return ports;
}

void vLoadPlatformData( int processorNumber )
{
    int i;
    int NCPUS=NUMBER_OF_CPUS;
    int NCPUS_X=NUMBER_OF_CPUS_X;
    int NCPUS_Y=NUMBER_OF_CPUS_Y;
    int HCLUSTER=HEIGHT_CLUSTER;
    int WCLUSTER=WIDTH_CLUSTER;
    int NCLUSTERS=NUMBER_OF_CLUSTERS;
    int GMASTER=GLOBAL_MASTER;
    int MAXTASKS=MAX_LOCAL_TASKS;
    int NOCBUFFER=NOC_BUFFER;
    unsigned int apps = NUMBER_OF_APPS;

    apps = appstype[0];
    apps = apps_addresses[0];
    apps = repository[0];
    apps = NUMBER_OF_APPS;

    opProcessorWrite( processors[ processorNumber ] ,
                      CPUS_NUM ,
                      &NCPUS ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors [ processorNumber ] ,
                      X_CPUS ,
                      &NCPUS_X ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      Y_CPUS ,
                      &NCPUS_Y ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      H_CLUSTER ,
                      &HCLUSTER ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      W_CLUSTER ,
                      &WCLUSTER ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      N_CLUSTER ,
                      &NCLUSTERS ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      G_MASTER ,
                      &GMASTER ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      L_TASKS ,
                      &MAXTASKS ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );

    opProcessorWrite( processors[ processorNumber ] ,
                      NBUFFER ,
                      &NOCBUFFER ,
                      sizeof( int ) ,
                      1 ,
                      True ,
                      OP_HOSTENDIAN_TARGET );
    
    //load repository information
    if (processorNumber==GLOBAL_MASTER) {
        opProcessorWrite( processors[ processorNumber ] ,
                          APPS_NUM ,
                          &apps ,
                          sizeof( apps ) ,
                          1 ,
                          True ,
                          OP_HOSTENDIAN_TARGET );

        for ( i = 0 ; i < apps; i++ ) {

            opProcessorWrite( processors[ processorNumber ] ,
                              APPS_TYPE + ( i * 4 ) ,
                              &appstype[ i ] ,
                              sizeof( appstype[ 0 ] ) ,
                              1 ,
                              True ,
                              OP_HOSTENDIAN_TARGET );
        }

        for ( i = 0 ; i < ( sizeof( apps_addresses ) / 4 ) ; i++ ) {

            opProcessorWrite( processors[ processorNumber ] ,
                              APPS_ADDR + ( i * 4 ) ,
                              &apps_addresses[ i ] ,
                              sizeof( apps_addresses[ 0 ] ) ,
                              1 ,
                              True ,
                              OP_HOSTENDIAN_TARGET );
        }

        for ( i = 0 ; i < (sizeof(repository)/4); i++ ) {

            opProcessorWrite( processors[ processorNumber ] ,
                              REPO_ADDR + ( i * 4 ) ,
                              &repository[ i ] ,
                              sizeof( repository[ 0 ] ) ,
                              1 ,
                              True ,
                              OP_HOSTENDIAN_TARGET );
        }
    }
}


int RouterAddress( int router )
{
    int r_address , pos_x , pos_y;

    pos_x = ( router % NUMBER_OF_CPUS_X ) << 8;
    pos_y = router / NUMBER_OF_CPUS_X;

    r_address = pos_x | pos_y;
    return r_address;
}

#ifdef OVP_REPORT
void update_volume( int xi , int yi , int xf , int yf , int vol )
{
    int xx = xi;
    int yy = yi;
    while ( 1 ) {
        if ( xf == xi ) {
            if ( yf>yi ) {
                yy++;
            }
            if ( yf<yi ) {
                yy--;
            }
        } else {
            if ( xf>xi ) { 
                xx++;
            } else {
                if ( xf<xi ) {
                    xx--;
                }
            }
        }

        xi = xx;
        yi = yy;

        volume[ NormalAddress( xi * 256 + yi ) ]
            = volume[ NormalAddress( xi * 256 + yi ) ]
              + vol;

        router_cycles[ NormalAddress( xi * 256 + yi ) ]
            = router_cycles[ NormalAddress( xi * 256 + yi ) ]
              + vol + 3;

        energy[ NormalAddress( xi * 256 + yi ) ]
            = energy[NormalAddress( xi * 256 + yi ) ]
              + ( vol * 0.0035104 );

        if ( ( xi == xf ) && ( yi == yf ) ) {
            break;
        }
    }
}
#endif //OVP_REPORT

void dmaProcess( int p )
{
    int data [ NUMBER_OF_CPUS ] ;
    while ( dma_size[ p ] ) {
        if ( dma_op[ p ] == READ ) {

            opProcessorRead( processors[ p ] ,
                             dma_address[ p ] ,
                             &data[ p ] ,
                             4 ,
                             1 ,
                             True ,
                             OP_HOSTENDIAN_LITTLE );

            dma_address[ p ] = dma_address[ p ] + 4;
            counter_send[ p ]++;
#ifdef OVP_REPORT
            if ( counter_send[ p ] == 1 ) {
                x_destiny[ p ] = data[ p ] >> 8;
                y_destiny[ p ] = data[ p ] & 0xFF;
                x_origin[ p ] = RouterAddress( p ) >> 8;
                y_origin[ p ] = RouterAddress( p ) & 0xFF;
                hops[ p ] = ( abs( x_destiny[ p ] - x_origin[ p ] )
                            + abs( y_destiny[ p ] - y_origin[ p ] ) );
            }
#endif //OVP_REPORT
            if ( counter_send[ p ] == 2 ) {
                size_send[ p ] = data[ p ] ;
#ifdef OVP_REPORT
                totalVolume = totalVolume + size_send[ p ] + 2;
                totalEnergy
                    = totalEnergy
                        + hops[ p ]
                        * ( size_send[ p ] + 2 )
                        * 0.0035104;
                volume[ p ] = volume[ p ] + size_send[ p ] + 2;
                energy[ p ]
                    = energy[ p ]
                        + ( size_send[ p ] + 2 )
                        * 0.0035104;
                router_cycles[ p ]
                    = router_cycles[ p ] + ( size_send[ p ] + 5 );
                total_hops = total_hops + hops[ p ];
                update_volume( x_origin[ p ] ,
                               y_origin[ p ] ,
                               x_destiny[ p ] ,
                               y_destiny[ p ] ,
                               size_send[ p ] + 2 );
#endif //OVP_REPORT
            }

#ifdef OVP_REPORT
            if(counter_send[ p ]==3) {
                if( ( ( data[ p ] ) == NI_HANDLER_MESSAGE_DELIVERY ) ||
                    ( ( data[ p ] ) == NI_HANDLER_MESSAGE_REQUEST ) ) {
                    total_hops_msg += hops[ p ];
                    totalVolume_msg += size_send[ p ] + 2;
                    totalEnergy_msg
                        += hops[ p ]
                        * ( size_send[ p ] + 2 )
                        * 0.0035104;
                }
            }
#endif // OVP_REPORT
            if ( counter_send[ p ]>2 ) {
                size_send[ p ]--;
                if ( size_send[ p ] == 0 ) {
                    counter_send[ p ] = 0;
                }
            }
            opNetWrite( data_read[ p ][ LOCAL ] , data[ p ] );
            dma_size[ p ]--;
        } else {

            if ( first[ p ] != NULL ) {

                if ( first_read[ p ] == 1 ) {
                    free( clean[ p ] );
                } else {
                    first_read[ p ] = 1;
                }
                data[ p ] = first[ p ]->x;

                opProcessorWrite( processors[ p ] ,
                                  dma_address[ p ] ,
                                  &data[ p ] ,
                                  4 ,
                                  1 ,
                                  False ,
                                  OP_HOSTENDIAN_TARGET );

                counter_packet[ p ]++;

                if ( counter_packet[ p ] == 2 ) {
                    size_packet[ p ] = data[ p ];
                }

                if ( counter_packet[ p ] > 2 ) {
                    size_packet[ p ]--;
                    if ( size_packet[ p ] == 0 ) {
                        counter_packet[ p ] = 0;
                        packets_out[ p ]++;
                    }
                }
                dma_address[ p ] = dma_address[ p ] + 4;
                dma_size[ p ]--;
                clean[ p ] = first[ p ];
                first[ p ] = first[ p ]->next;
            } else {
                first_read[ p ] = 0;
            }
        }
    }
    if ( dma_size[ p ] == 0 ) {
        dma_start[ p ] = 0;
    }
}

//function acessed when a register is read from memory
int register_bank_read( int address , int p )
{
    int return_data;

    switch ( address ) {
        case READ_DMA_READY:
            if ( dma_start[ p ] == 1 ) {
                return_data = dma_size[ p ];
            } else {
                return_data = 0;
            }
            break;

        case READ_NI_READY:
            if ( first[ p ] != NULL ) {
                if ( first_read[ p ] == 1 ) {
                    free( clean[ p ] );
                } else {
                    first_read[ p ] = 1;
                }
                data_in[ p ] = first[ p ]->x ;
                counter_packet[ p ]++;
                if ( counter_packet[ p ] == 2 ) {
                    size_packet[ p ] = first[ p ]->x;
                }
                if ( counter_packet[ p ] > 2 ) {
                    size_packet[ p ]--;
                    if ( size_packet[ p ] == 0 ) {
                        counter_packet[ p ]=0;
                        packets_out[ p ]++;
                    }
                }
                header_dma_size[ p ]--;
                clean[ p ] = first[ p ];
                first[ p ] = first[ p ]->next;
            } else {
                first_read[ p ] = 0;
            }
            return_data = header_dma_size[ p ];
            break;

        case READ_DATA_IN:
                return_data = data_in[ p ];
            break;

        case READ_TICK_COUNTER:
            return_data
                = ( ( float ) opModuleCurrentTime( platformMod ) )
                    * 1000000000;
            break;

        case READ_INITIALIZE_ROUTER:
            return_data = p;
            opNetWrite( router_address[ p ] , RouterAddress( p ) );
            vLoadPlatformData(p);
            break;

        case NOC_SIZE_X:
            return_data = NUMBER_OF_CPUS_X ;
            break;

        case NOC_SIZE_Y:
            return_data = NUMBER_OF_CPUS_Y ;
            break;

        default: break;
    }

    if(packets_in[ p ] == packets_out[ p ]) {
        opNetWrite( ni_intr[ p ], 0 );
    }
    
    return ( return_data );
}

//function acessed when a register is written in memory
void register_bank_write( int address , int value , int p )
{
    switch ( address ) {

        case WRITE_PRINT_OUT:
#ifdef DEBUG
            opPrintf( "%c" , value );
#endif //DEBUG
#ifdef OVP_REPORT
            char t_string[255];
            sprintf( t_string , "log/PE%d.txt" , p );
            FILE* fp;
            fp = fopen( t_string , "a" );
            fprintf( fp , "%c" , value );
            fclose( fp );
#endif //OVP_REPORT
            break;

        case WRITE_DMA_SIZE:
            dma_size[ p ] = value;
            break;

        case WRITE_DMA_SIZE_2:
            dma_size_2[ p ] = value;
            dma_2[ p ] = 1;
            break;

        case WRITE_DMA_ADDRESS:
            dma_address[ p ] = value;
            break;

        case WRITE_DMA_ADDRESS_2:
            dma_address_2[ p ] = value;
            dma_2[ p ] = 1;
            break;

        case WRITE_DMA_OP:
            dma_op[ p ] = value;
            break;

        case WRITE_DMA_START:
            first_address[ p ] = dma_address[ p ];
            dma_start[ p ] = 1;
            counter_packet[ p ] = 0;
            dmaProcess( p );
            if ( dma_2[ p ] == 1 ) {
                dma_address[ p ] = dma_address_2[ p ];
                dma_size[ p ] = dma_size_2[ p ];
                first_address[ p ] = dma_address[ p ];
                dma_start[ p ] = 1;
                dmaProcess( p );
                dma_2[ p ] = 0;
            }
            dma_size_2[ p ] = 0;
            dma_size[ p ] = 0;
            break;

        case WRITE_START_READ_NI:
            header_dma_size[ p ] = 1;
            break;

        case WRITE_NI:
            opNetWrite( data_read[ p ][LOCAL] , value );
            counter_send[ p ]++;
#ifdef OVP_REPORT
            if ( counter_send[ p ] == 1 ) {
                x_destiny[ p ] = value >> 8;
                y_destiny[ p ] = value & 0xFF;

                x_origin[ p ] = RouterAddress( p ) >> 8;
                y_origin[ p ] = RouterAddress( p ) & 0xFF;

                hops[ p ] = ( abs( x_destiny[ p ]-x_origin[ p ] )+abs( y_destiny[ p ]-y_origin[ p ] ) );
            }
#endif //OVP_REPORT
            if ( counter_send[ p ] == 2 ) {
                size_send[ p ] = value;
            }
            if ( counter_send[ p ]>2 ) {
                size_send[ p ]--;
                if ( size_send[ p ] == 0 ) {
                    counter_send[ p ] = 0;
                }
            }
            break;

        case WRITE_INITIALIZE_ROUTER:
            opNetWrite( router_address[value] , RouterAddress( value ) );
            break;

        case WRITE_END_SIM: {
                
                gettimeofday( &stop , NULL );
                switch ( value ) {

                    case SIM_SUCCESS:
                        {
                            
                            opNetWrite( end_sim[ p ] , value );
                            
                            if ( p == GLOBAL_MASTER ) {
                                opPrintf( "END SIMULATION %d\n" , p );
#ifdef OVP_REPORT
                                int x , y;
                                totalSimulated = 0;
                                for ( int stepIndex=0; stepIndex < NUMBER_OF_CPUS; stepIndex++ ) {
                                    totalSimulated+= opProcessorICount( processors[stepIndex] );
                                    if ( opProcessorICount( processors[stepIndex] )>max_instructions ) {
                                        max_instructions = opProcessorICount( processors[stepIndex] );
                                    }
                                }
                                opPrintf( "Total communication volume:" );

                                for ( y=( NUMBER_OF_CPUS_Y-1 ); y>-1; y-- ) {
                                    opPrintf( "\n" );
                                    for ( x=0; x<NUMBER_OF_CPUS_X; x++ )
                                        opPrintf( "%d\t" , volume[NormalAddress( x*256+y )] );
                                }
                                opPrintf( "\n" );

                                opPrintf( "Total communication energy:" );
                                for ( y=( NUMBER_OF_CPUS_Y-1 ); y>-1; y-- ) {
                                    opPrintf( "\n" );
                                    for ( x=0; x<NUMBER_OF_CPUS_X; x++ )
                                        opPrintf( "%f\t" , energy[NormalAddress( x*256+y )] );
                                }
                                opPrintf( "\n" );

                                opPrintf( "Router activity in clock cycles :" );
                                for ( y=( NUMBER_OF_CPUS_Y-1 ); y>-1; y-- ) {
                                    opPrintf( "\n" );
                                    for ( x=0; x<NUMBER_OF_CPUS_X; x++ )
                                        opPrintf( "%d\t" , router_cycles[NormalAddress( x*256+y )] );
                                }
                                opPrintf( "\n" );

                                opPrintf( "Router energy active:" );
                                for ( y=( NUMBER_OF_CPUS_Y-1 ); y>-1; y-- ) {
                                    opPrintf( "\n" );
                                    for ( x=0; x<NUMBER_OF_CPUS_X; x++ ) {
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 3 )
                                            opPrintf( "%f\t" , router_cycles[NormalAddress( x*256+y )] * 3.81 );
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 4 )
                                            opPrintf( "%f\t" , router_cycles[NormalAddress( x*256+y )] * 4.30 );
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 5 )
                                            opPrintf( "%f\t" , router_cycles[NormalAddress( x*256+y )] * 4.79 );
                                    }
                                }
                                opPrintf( "\n" );
                                opPrintf( "Router energy idle:" );

                                for ( y=( NUMBER_OF_CPUS_Y-1 ); y>-1; y-- ) {
                                    opPrintf( "\n" );
                                    for ( x=0; x<NUMBER_OF_CPUS_X; x++ ) {
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 3 )
                                            opPrintf( "%f\t" , ( max_instructions - router_cycles[NormalAddress( x*256+y )] ) * 1.97 );
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 4 )
                                            opPrintf( "%f\t" , ( max_instructions - router_cycles[NormalAddress( x*256+y )] ) * 2.46 );
                                        if ( RouterPorts( NormalAddress( x*256+y ) ) == 5 )
                                            opPrintf( "%f\t" , ( max_instructions - router_cycles[NormalAddress( x*256+y )] ) * 2.94 );
                                    }
                                }
                                opPrintf( "\n" );

                                elapsed = (stop.tv_sec - start.tv_sec) +  ((stop.tv_usec - start.tv_usec)/1000000.0);
                                mips = totalSimulated/(elapsed*1000000);
                                opPrintf("MAX INSTRUCTIONS = %f\n", max_instructions);
                                opPrintf("TOTAL SIMULATED INSTRUCTIONS = %f\n", totalSimulated);
                                opPrintf("TOTAL ELAPSED TIME = %f\n", elapsed);
                                opPrintf("TOTAL MIPS = %f\n", mips);
                                opPrintf("TOTAL COMMUNICATION VOLUME = %d\n", totalVolume);
                                opPrintf("TASK COMMUNICATION VOLUME = %d\n", totalVolume_msg);
                                opPrintf("TOTAL COMMUNICATION ENERGY = %f\n", totalEnergy);
                                opPrintf("TASK COMMUNICATION ENERGY = %f\n", totalEnergy_msg);
                                opPrintf("TOTAL COMMUNICATION HOPS = %d\n", total_hops);
                                opPrintf("TASK COMMUNICATION HOPS = %d\n", total_hops_msg);
#ifdef MIPS_REPORT
                                FILE * pFile; 
                                pFile = fopen ("mips.txt","a"); 
                                fprintf(pFile,"CPUS %d; MEM %d; TIME %lf; INSTRUCTIONS %llu; MIPS %lf;\n",NUMBER_OF_CPUS,SIZE_BUFFER,elapsed,(long long unsigned int)totalSimulated,mips);
                                fclose (pFile);
#endif //MIPS_REPORT
#endif //OVP_REPORT
                                opInterrupt();
                            }
                            break;
                        }
                    
                    case IRQ_FAULT:
                        {
                            opPrintf("Error: Fault %d\n", p );
                            opInterrupt();
                            break;
                        }

                    case IRQ_NMI:
                        {
                            opPrintf("Error: NMI %d\n", p );
                            opInterrupt();
                            break;
                        }

                    case MALLOC_FAILED:
                        {
                            opPrintf("Error: vApplicationMallocFailedHook %d\n", p );
                            opInterrupt();
                            break;
                        }

                    case STACK_OVERFLOW:
                        {
                            opPrintf("Error: vApplicationStackOverflowHook %d\n", p );
                            opInterrupt();
                            break;
                        }

                    case SYSCALL_ERROR:
                        {
                            opPrintf("Error: System Call not recognized %d\n", p );
                            opInterrupt();
                            break;
                        }

                    case NI_HANDLER_ERROR:
                        {
                            opPrintf("Error: NI Service not recognized %d\n", p );
                            opInterrupt();
                            break;
                        }
                }
                break;
            }

        default: break;
    }
}
