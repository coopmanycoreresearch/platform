///System includes
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stddef.h>

///Local includes from OVPSim
#include "op/op.h"
#include "repository.h"

/**  NoC variables **/
optNetP router_address      [ NUMBER_OF_CPUS ] ;
optNetP end_sim             [ NUMBER_OF_CPUS ] ;
optNetP ni_intr             [ NUMBER_OF_CPUS ] ;
optNetP data_read           [ NUMBER_OF_CPUS ] [ NPORT ] ;
optNetP data_write          [ NUMBER_OF_CPUS ] [ NPORT ] ;
optProcessorP processors    [ NUMBER_OF_CPUS ] ;
optPeripheralP pse          [ NUMBER_OF_CPUS ] ;
optMemoryP mem              [ NUMBER_OF_CPUS ] ;
optBusP bus                 [ NUMBER_OF_CPUS ] ;

//Main struct pointer
struct processorStruct* processorData;

//PE wrapper
#define PE processorData[processorNumber]

// Flags
Bool removeDumpFiles = True;

// temporary string array used to create components' labels
char nameString[64];

//Trace Flags
Bool enableFunctionProfile  = False;
Bool enableLineCoverage     = False;
Bool enableContexTrace      = False;

// Temporary string
#define STRING_SIZE 256

char tempString     [ STRING_SIZE ] ;
char modelVariant   [ STRING_SIZE ] ;

//File pointer
FILE* filePointer;

//Options
struct optionsS _options;

//Top level module
optModuleP topLevelModule;
optModuleP platformMod;

// temporary string array used to create components' labels
char nameString[64];

/////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////

//
// simulation management
//
void simulate( struct optionsS _options , optModuleP _mi ) {
    /// platform options
    options=_options;

    /// top level module
    topLevelModule = _mi;

    /// constructor
    initialize();

    ///
    /// simulation flow
    ///
    opRootModuleSimulate( topLevelModule );

}

//
// Constructor and options checker
//
void initialize() {
    /// allocate the array according with the number of DUTs
    processorData = ( processorDataP ) malloc ( sizeof( processorDataS ) * MACRO_NUMBER_OF_CPUS );

    //Get the platform module
    sprintf( tempString , "platform" );

    platformMod = opObjectByName( topLevelModule , tempString , OP_MODULE_EN ).Module;

    optProcessorP ch = 0;
    while( ( ch = opProcessorNext( platformMod , ch ) ) ) {
        initProcessorInst( ch );
    }

#ifdef NoC_Enable
    int processorNumber;

    for ( processorNumber = 0; processorNumber < NUMBER_OF_CPUS; processorNumber++ ) {
        // opPrintf( "RouterAddress(%d) 0x%02x\n" ,
                // processorNumber ,
                // RouterAddress( processorNumber ) );

        //EAST CONNECTION
        if (       RouterPosition( processorNumber ) != BOTTOM_RIGHT
                && RouterPosition( processorNumber ) != CENTER_RIGHT
                && RouterPosition( processorNumber ) != TOP_RIGHT
           ) {
            /* opPrintf( "East\n" ); */
            sprintf( nameString , "data_read-%d-east" , processorNumber );
            data_read[processorNumber][EAST] = opNetNew( PE.modObj , nameString , 0 , 0 );
            opObjectNetConnect( pse[processorNumber]     , data_read[processorNumber][EAST] , "data_read_0" );
            opObjectNetConnect( pse[processorNumber + 1] , data_read[processorNumber][EAST] , "data_write_1" );
        }

        //WEST CONNECTION
        if (       RouterPosition( processorNumber ) != BOTTOM_LEFT
                && RouterPosition( processorNumber ) != CENTER_LEFT
                && RouterPosition( processorNumber ) != TOP_LEFT
           ) {
            /* opPrintf( "West\n" ); */
            sprintf( nameString , "data_read-%d-west" , processorNumber );
            data_read[processorNumber][WEST] = opNetNew( PE.modObj , nameString , 0 , 0 );
            opObjectNetConnect( pse[processorNumber]     , data_read[processorNumber][WEST] , "data_read_1" );
            opObjectNetConnect( pse[processorNumber - 1] , data_read[processorNumber][WEST] , "data_write_0" );
        }

        //NORTH CONNECTION
        if (       RouterPosition( processorNumber ) != TOP_LEFT
                && RouterPosition( processorNumber ) != TOP_CENTER
                && RouterPosition( processorNumber ) != TOP_RIGHT
           ) {
            /* opPrintf( "North\n" ); */
            sprintf( nameString , "data_read-%d-north" , processorNumber );
            data_read[processorNumber][NORTH] = opNetNew( PE.modObj , nameString , 0 , 0 );
            opObjectNetConnect( pse[processorNumber]                    , data_read[processorNumber][NORTH] , "data_read_2" );
            opObjectNetConnect( pse[processorNumber + NUMBER_OF_CPUS_X] , data_read[processorNumber][NORTH] , "data_write_3" );
        }

        //SOUTH CONNECTION
        if (       RouterPosition( processorNumber ) != BOTTOM_LEFT
                && RouterPosition( processorNumber ) != BOTTOM_CENTER
                && RouterPosition( processorNumber ) != BOTTOM_RIGHT
           ) {
            /* opPrintf( "South\n" ); */
            sprintf( nameString , "data_read-%d-south" , processorNumber );
            data_read[processorNumber][SOUTH] = opNetNew( PE.modObj , nameString , 0 , 0 );
            opObjectNetConnect( pse[processorNumber]                    , data_read[processorNumber][SOUTH] , "data_read_3" );
            opObjectNetConnect( pse[processorNumber - NUMBER_OF_CPUS_X] , data_read[processorNumber][SOUTH] , "data_write_2" );
        }

        opNetWriteMonitorAdd( data_write[processorNumber][LOCAL] , Receive , ( void * ) ( intptr_t ) processorNumber );
    }
#endif

}

//
// initialise ONE processor
//
void initProcessorInst( optProcessorP processorObj ) {
    // opPrintf( "processorObj name %s\n" , opObjectName( processorObj ) ); // Debug
    Uns32 processorNumber;

    // Get processor ID
    sscanf( opObjectName( processorObj ) , "cpu%i" , &processorNumber );
    PE.identifier = processorNumber;

    //Get PE handlers for the processor and memory object
    PE.processorObj = processorObj; // Get the first processor
    PE.options      = options;      // Platform options
    PE.modObj       = platformMod;

    optBusP obj = 0;
    for ( int i = 0 ; i <= processorNumber ; i++ ) {
        obj = opBusNext( PE.modObj , obj );
    }
    // opPrintf( "found bus called '%s'\n" , opObjectName( obj ) ); // Debug

    bus[processorNumber] = obj;
    processors[processorNumber] = processorObj;

#ifdef NoC_Enable
    // opPrintf( "NoC_Enable\n" ); // Debug

    sprintf( nameString , "extMemReg-%d" , processorNumber );
    opBusSlaveNew( bus[processorNumber] , nameString , 0 , OP_PRIV_RW , CB_READ_LOW , CB_READ_HIGH , regbank_rb , NULL , 0 , 0 );
    opBusSlaveNew( bus[processorNumber] , nameString , 0 , OP_PRIV_RW , CB_WRITE_LOW , CB_WRITE_HIGH , NULL , regbank_wb , 0 , 0 );

    sprintf( nameString , "router-%d" , processorNumber );
    pse[processorNumber] = opPeripheralNew( platformMod , "./NoC.pse" , nameString , NULL , NULL );

    sprintf( nameString , "router_address-%d" , processorNumber );
    router_address[processorNumber] = opNetNew( PE.modObj , nameString , 0 , 0 );
    opObjectNetConnect( pse[processorNumber] , router_address[processorNumber] , "router_address" );

    sprintf( nameString , "end_sim-%d" , processorNumber );
    end_sim[processorNumber] = opNetNew( PE.modObj , nameString , 0 , 0 );
    opObjectNetConnect( pse[processorNumber] , end_sim[processorNumber] , "end_sim" );

    sprintf( nameString , "data_read-%d-local" , processorNumber );
    data_read[processorNumber][LOCAL] = opNetNew( PE.modObj , nameString , 0 , 0 );
    opObjectNetConnect( pse[processorNumber] , data_read[processorNumber][LOCAL] , "data_read_4" );

    sprintf( nameString , "data_write-%d-local" , processorNumber );
    data_write[processorNumber][LOCAL] = opNetNew( PE.modObj , nameString , 0 , 0 );
    opObjectNetConnect( pse[processorNumber] , data_write[processorNumber][LOCAL] , "data_write_4" );

    //Interuption
    sprintf( nameString , "NI_INT-%d" , processorNumber );
    ni_intr[processorNumber] = opNetNew( PE.modObj , nameString , 0 , 0 );

#ifdef M0
    opObjectNetConnect( PE.processorObj , ni_intr[processorNumber] , "int11" );
#else
    opObjectNetConnect( PE.processorObj , ni_intr[processorNumber] , "int14" );
#endif // M0

#endif
    
}
